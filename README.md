# Web Application Programming

## Computer Science - 1st Cycle Degree of Studies - Merito University, Poznan


### Introduction

The code in the repository has been prepared as a support for classes carried out at Merito University in Poznan, as part of the 1st Cycle Degree of Studies in the field of Computer Science.

The material should be used in conjunction with additional explanations, for example from the teacher.

The attached code should not be used in production environments due to the simplifications it contains.


### Sources

Detailed references to the sources of which the attached examples were based on can be found on the Moodle platform and in the materials presented during the classes.

Selected sources below:
* https://angular.io/start, Google, 2010 - 2024
* https://angular.io/tutorial, Google, 2010 - 2024
* https://www.digitalocean.com/community/tutorials/how-to-build-a-weather-app-with-angular-bootstrap-and-the-apixu-api, DigitalOcean, LLC, 2019


### Running the project

In order to run a project, first make sure that Node (https://nodejs.org/en/download/)
and Angular CLI (https://angular.io/cli) are installed.

Then, the following commands can be executed from within the CLI:
1. *cd weather-app*
2. *npm install*
3. *ng serve*
